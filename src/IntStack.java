import java.util.LinkedList;
import java.util.StringTokenizer;

public class IntStack {

   public static void main (String[] argum) {
      System.out.println(interpret("-12 4 + -"));
   }

   private LinkedList<Integer> stack;

  /*IntStack() {
      this(10);
   }*/

   IntStack() {
      stack = new LinkedList<Integer>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      IntStack newStack = new IntStack(); //Loon uue stacki, kuhu kloonin olemasoleva.
      if (!stEmpty()) {
         for (int i = 0; i<stack.size(); i++) {
            newStack.stack.add(i, stack.get(i));
         }
      }
      return newStack; // Tagastan uue stacki.
   }

   public boolean stEmpty() { //Kontroll, kas on tühi
      return (stack.size()==0);
   } //stEmpty

   /*public boolean stFull() { //Kontroll, kas on täis
      return (top+1 == stack.size());
   } //stFull*/

   public void push (int a) {
      stack.add(new Integer(a));
   } //push

   public int pop() {
      if (stEmpty()) throw new IndexOutOfBoundsException("Stack is empty!");
      return stack.removeLast();
   } // pop

   public void op (String s) {
      if (!stEmpty()) {
         if (s.equals("+")) push(pop()+pop());
         else if (s.equals("-")) push(-pop()+pop());
         else if (s.equals("*")) push(pop()*pop());
         else if (s.equals("/")) {
            int elem = pop();
            push(pop() / elem);
         } else throw new RuntimeException("Tundmatu operaator!" + s);
      }
   } //op

   public int tos() {
      if (!stEmpty()) return stack.getLast();
      else throw new RuntimeException("Tühi stack!");
   } //tos

   @Override
   public boolean equals (Object o) {
      if (((IntStack)o).stack.size() != stack.size()) return false;
      if (((IntStack)o).stEmpty() && stEmpty()) return true;
      for (int i = 0; i<stack.size(); i++) {
         if (((IntStack)o).stack.get(i).compareTo(stack.get(i))!=0) return false;
      }
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty()) return "Empty";
      StringBuffer string = new StringBuffer();
      for (int i = 0; i<stack.size(); i++) {
         string.append(String.valueOf(stack.get(i)) + " ");
      }
      return string.toString();
   }

   public static int interpret (String pol) { //Algoritmiga tutvusin: http://rosettacode.org/wiki/Parsing/RPN_calculator_algorithm#Java_2
      StringTokenizer st = new StringTokenizer(pol);
      if (!st.hasMoreElements()) throw new RuntimeException("Viga sisendiga: "+pol+" | Tühi sisend!");
      IntStack numbers = new IntStack();
      while (st.hasMoreElements()) {
         Object elem = st.nextElement();
         try {
            int num = Integer.parseInt(elem.toString());
            numbers.push(num);
         } catch (NumberFormatException e) { //Esineb operaator
            if (numbers.stack.size()<2) throw new RuntimeException("Viga sisendiga: "+pol+" | Samm: "+elem.toString());
            numbers.op(elem.toString());
         }
      }
      if (numbers.stack.size()<2) return numbers.pop();
      else throw new RuntimeException("Viga sisendiga: "+pol+" | Puudub piisavalt arve edaspidiseks arvutamiseks!");
   }
}